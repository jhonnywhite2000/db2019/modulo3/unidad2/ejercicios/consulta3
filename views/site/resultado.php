<?php
use yii\grid\GridView;
?>

<div class="jumbotron">
        <h1><?=$titulo;?></h1>
        <p class="lead"><?=$enunciado;?></p>
        <p class="lead"><?=$sql;?></p>
</div>



<?= GridView::widget([
    'dataProvider' => $resultados,
    'columns' => $campos,
]);
?>
