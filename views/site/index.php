<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de selección 3!</h1>
        <p class="lead">Modulo 3 Unidad 2</p>
    </div>

    <div class="body-content">
         <div class="row">
             <!--
             Boton de consulta
             -->
             
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 1</h3>
                    <p>Edad de los ciclistas de banesto
                    
                    </p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta1a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta1'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            <!--
             Boton de consulta dos
             -->
             
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 2</h3>
                    <p>Edad de los ciclistas de banesto o navigare</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta2a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta2'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
             
             <!--
             Boton de consulta tres
             -->
             
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 3</h3>
                    <p>Dorsal de los ciclistas de banesto y edad entre 25 y 32</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta3a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta3'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
         </div>   
        <div class="row">
             <!--
             Boton de consulta
             -->
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 4</h3>
                    <p>Dorsal de los ciclistas de banesto o edad entre 25 y 32</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta4a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta4'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            
               <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 5</h3>
                    <p>Ciclistas cuyo nombre comience con R</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta5a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta5'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
             <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 6</h3>
                    <p>Codigo de las etapas que su salida y llegada sean en la misma poblacion</p>
                    <?= Html::a('Active record', ['site/consulta6a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta6'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
         </div> 
        <div class="row">
             <!--
             Boton de consulta
             -->
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 7</h3>
                    <p>Codigo de las etapas que su salida y llegada no sean de la misma poblacion y que s econozca el dorsal del ciclista que ha ganadado la etapa</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta7a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta7'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 8</h3>
                    <p>Nombre de los puertos cuya altura este entre 1000 y 2000 o que la altura sea mayor que 2400</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta8a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta8'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
             <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 9</h3>
                    <p>Nombre de los equipos que tengan mas de cuatro ciclistas</p>
                    <?= Html::a('Active record', ['site/consulta9a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta9'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
         </div> 
        <div class="row">
             <!--
             Boton de consulta
             -->
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 10</h3>
                    <p>Nombre de los equipos que tengan mas de cuatro ciclistas cuya edad es entre 28 y 32</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta10a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta10'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 11</h3>
                    <p>Indicar el numero de estapas que ha ganado cada ciclista</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta11a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta11'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 12</h3>
                    <p>Indicar el dorsal de los clclistas que han ganado mas de una etapa</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta12a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta12'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
        </div> 
        <div class="row">
             <!--
             Boton de consulta
             -->
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 13</h3>
                    <p>Nombre de los equipos que tengan mas de cuatro ciclistas cuya edad es entre 28 y 32</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta13a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta13'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 14</h3>
                    <p>Indicar el numero de estapas que ha ganado cada ciclista</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta14a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta14'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 15</h3>
                    <p>Indicar el dorsal de los clclistas que han ganado mas de una etapa</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta15a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta15'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
        </div>
        <div class="row">
             <!--
             Boton de consulta
             -->
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 16</h3>
                    <p>Nombre de los equipos que tengan mas de cuatro ciclistas cuya edad es entre 28 y 32</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta16a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta16'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 17</h3>
                    <p>Indicar el numero de estapas que ha ganado cada ciclista</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta17a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta17'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 18</h3>
                    <p>Lista el dorsal del ciclista con el código de maillo y cuantas veces ese ciclista ha llevado ese maillot</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta18a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta18'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
        </div>
        <div class="row">
             <!--
             Boton de consulta
             -->
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta adicional 18</h3>
                    <p>Nombre de los equipos que tengan mas de cuatro ciclistas cuya edad es entre 28 y 32</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta19'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta19a'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
